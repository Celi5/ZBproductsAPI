import { gCall } from "../test.utils/gCall";
import { Connection } from "typeorm";
import { testConn } from "../test.utils/testConn";
import faker from "faker";
import { Brand } from "../entity/Brand";

let conn: Connection;
beforeAll(async () => {
  conn = await testConn();
});
afterAll(async () => {
  conn.close();
});

const createBrandMutation = `
mutation CreateBrandMutation($brand: BrandInput!) {
    createBrand(brand: $brand) {
      name
      description
    }
  }`;
describe("BrandResolver", () => {
  it("create Brand", async () => {
    const brand = {
      name: faker.internet.avatar(),
      description: faker.lorem.text(),
    };
    const response = await gCall({
      source: createBrandMutation,
      variableValues: {
        brand,
      },
    });
    expect(response).toMatchObject({
      data: {
        createBrand: {
          name: brand.name,
          description: brand.description,
        },
      },
    });
    const dbBrand = await Brand.findOne({ where: { name: brand.name } });
    expect(dbBrand).toBeDefined();
  });
});
