import { gCall } from "../test.utils/gCall";
import { Connection } from "typeorm";
import { testConn } from "../test.utils/testConn";
import faker from "faker";
import { Product } from "../entity/Product";
import { Brand } from "../entity/Brand";

let conn: Connection;
beforeAll(async () => {
  conn = await testConn();
});
afterAll(async () => {
  conn.close();
});

const createProductMutation = `
mutation CreateProductMutation($product: CreateProductInput!) {
    createProduct(product: $product) {
      sku
      name
      description
    }
  }`;
describe("ProductResolver", () => {
  it("create Product", async () => {
    const brand = await Brand.create({
      name: "Tortas Filos",
      description: faker.lorem.sentence(),
    }).save();
    if (!brand) {
      throw new Error("Non valid idBrand for relations");
    }
    const product = {
      name: faker.lorem.word(),
      description: faker.lorem.sentence(),
      price: 100,
      sku: faker.random.word(),
      idBrand: brand.id,
    };
    const response = await gCall({
      source: createProductMutation,
      variableValues: {
        product,
      },
    });

    expect(response).toMatchObject({
      data: {
        createProduct: {
          sku: product.sku,
          name: product.name,
          description: product.description,
        },
      },
    });
    const dbProduct = await Product.findOne({ where: { sku: product.sku } });
    expect(dbProduct).toBeDefined();
  });
});
