import { gCall } from "../test.utils/gCall";
import { Connection } from "typeorm";
import { testConn } from "../test.utils/testConn";
import faker from "faker";
import { User } from "../entity/User";

let conn: Connection;
beforeAll(async () => {
  conn = await testConn();
});
afterAll(async () => {
  conn.close();
});

const signUpMutation = `
mutation SignUpMutation($user: UserInput!) {
    signUp(user: $user) {
    id
    firstName
    lastName
    username
    email  
    }
  }`;
describe("UserResolver", () => {
  it("create User", async () => {
    const user = {
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      email: faker.internet.email(),
      username: faker.internet.userName(),
      password: faker.internet.password(),
    };
    const response = await gCall({
      source: signUpMutation,
      variableValues: {
        user,
      },
    });

    expect(response).toMatchObject({
      data: {
        signUp: {
          firstName: user.firstName,
          lastName: user.lastName,
          username: user.username,
          email: user.email,
        },
      },
    });
    const dbUser = await User.findOne({ where: { email: user.email } });
    expect(dbUser).toBeDefined();
  });
});
