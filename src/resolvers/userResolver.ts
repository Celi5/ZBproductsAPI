import { User } from "../entity/User";
import { Arg, Ctx, Mutation, Query, Resolver } from "type-graphql";
import { UserInput, UserPwdInput } from "../inputs/userInput";
import { LoginResponse, UserResponse } from "../types/responses";
import { OwnContext } from "../types/context";
import { compare, hash } from "bcryptjs";
import { createAccessToken, createRefreshToken } from "../auth";

@Resolver(() => User)
export class UserResolver {
  @Query(() => User)
  public async getUser(@Arg("id") id: number): Promise<User | undefined> {
    const user = await User.findOne({
      where: [{ id: id }],
    });
    return user;
  }

  @Query(() => [User])
  public async getUsers(): Promise<User []| undefined> {
    const users = await User.find();
    return users;
  }

  @Mutation(() => User)
  public async signUp(
    @Arg("user") user: UserInput,
  ): Promise<User | undefined> {
    // const hashedPass = await argon2.hash(user.password);
    const hashedPass = await hash(user.password,12)
    user.password = hashedPass;
    const newUser = await User.create(user).save();
    
    
    return newUser;
  }

  // @Authorized()
  @Mutation(() => UserResponse)
  public async updateUser(
    @Arg("id") id: number,
    @Arg("user") user: UserInput
  ): Promise<UserResponse> {
    const dbUser = await User.findOne(id);
    if (!dbUser) {
      return {
        errors: [{ message: "Non existing user for given id" }],
      };
    }
    const updatedData = {
      ...dbUser,
      ...user,
    } as User;

    try {
      await User.update(id, updatedData);
      return {
        user: updatedData,
      };
    } catch (err) {
      console.error(err);
      return {
        errors: [{ message: "Unable to update user." }],
      };
    }
  }

  @Mutation(() => LoginResponse)
  public async signIn(
    @Arg("user") user: UserPwdInput,
    @Ctx() { res }: OwnContext
  ): Promise<LoginResponse> {
    const dbUser = await User.findOne(
      user.emailUsername.includes("@")
        ? { where: { email: user.emailUsername } }
        : { where: { username: user.emailUsername } }
    );
    if (dbUser) {
      // const checkPass = await argon2.verify(dbUser.password,user.password);
      const checkPass = await compare(user.password, dbUser.password);
      if(checkPass) {
        res.cookie('qid', createRefreshToken(dbUser),
        {
          httpOnly:true,
          sameSite: 'none',
          secure: false,
        })
        return{
          accessToken: createAccessToken(dbUser)
        }
      }
    }
    throw new Error ("Wrong credentials")
  }

  // @Mutation(() => Boolean)
  // logout(@Ctx() { req, res }: OwnContext) {
  //   return new Promise((resolve) =>
  //     req.session.destroy((err) => {
  //       res.clearCookie('qid');
  //       if (err) {
  //         console.log(err);
  //         resolve(false);
  //         return;
  //       }
  //       resolve(true);
  //     })
  //   );
  // }
}
