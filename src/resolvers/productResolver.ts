import { Product } from "../entity/Product";
import { CreateProductInput, ProductInput } from "../inputs/productInput";
import { Arg, Mutation, Query, Resolver, Ctx, UseMiddleware } from "type-graphql";
import { Brand } from "../entity/Brand";
import { ProductResponse } from "../types/responses";
import { sendEmail } from "../utils/sendEmail";
import { User } from "../entity/User";
import { OwnContext } from "../types/context";
import { isAuth } from "../auth";

@Resolver()
export class ProductResolver{
    @Query(() => [Product])
    async getProducts(): Promise<Product[]>{
        return Product.find({relations:["brand"]});
    }

    @Query(() => Product)
    async getProduct(@Arg("id") id:number):Promise<Product|undefined>{
        await this.updateTimesQueried(id);
        return Product.findOne(id,{relations:["brand"]});
    }

    async updateTimesQueried(id:number){
        const product = await Product.findOne(id);
        if(product){
            product.timesQueried++;
            const updatedData = {
                ...product
            }as Product
            try{
                await Product.update(id, updatedData);
            }catch(err){
                throw new Error ("Problem updating product")
            }
        } 
    }

    @Mutation(() => Product)
    async createProduct(@Arg ("product") product: CreateProductInput) : Promise<Product|undefined>{
        try{
            const brand = await Brand.findOne(product.idBrand);
            if(brand){
                const newProduct = Product.create(product)
                newProduct.brand = brand;
                await Product.save(newProduct);
                return newProduct;   
            }else{
                return undefined;
            }
        }catch(err){
            console.error("Error", err);
            return undefined;
        }
    }

    // @Authorized()
    @Mutation(() => ProductResponse)
    @UseMiddleware(isAuth)
    async updateProduct(
        @Arg ("id") id: number,
        @Arg ("product") product: ProductInput,
        @Ctx() {payload} : OwnContext): 
        Promise<ProductResponse>{
        
            let productUpdate = await Product.findOne(id);
            let brand;

            if(!productUpdate){
                return{ 
                    errors: [
                        { field: "id", message:"Non existing product with given id."}
                    ]
                }
            }
            if(product.idBrand){
                if(brand){
                    productUpdate.brand = brand;
                }else{
                    return{ 
                        errors: [{message: "Non existing brand with given id"}]
                    }
                }
            }
            const updatedData = {
                ...productUpdate,
                ...product,
            } as Product;
            try{
                await Product.update(id,updatedData);
                const idUser: number = +payload!.userId ;
                const dbUser = await User.findOne(idUser);
                const userMails = await this.getUsersEmail();
                if(!dbUser){
                    return{
                        errors:[{message:"Not authorized user"}]
                    }
                }
                const userName = dbUser.firstName + dbUser.lastName
                await sendEmail(userMails, userName,updatedData)

                const updatedProd = await Product.findOne(updatedData.id)
                console.log(updatedProd);
                if(!updatedProd){
                    return {
                        errors:[{message: "Not possible to update product"}]
                    }
                }else{
                return{ 
                    product: updatedProd
                }}
            }catch(err){
                console.log(err);
                return{ 
                    errors:[
                        {message: "Unable to update product: "+ err}
                    ]
                }
            }
        
            
            
        
    }

    async getUsersEmail():Promise<string[]>{
        const users = await User.find();
        let usersEmails : string[];
        usersEmails = []
        users.forEach(element => {
            usersEmails.push(element.email);
        });
        return usersEmails;
    }   
        
}
