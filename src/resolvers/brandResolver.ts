import { Brand } from "../entity/Brand";
import { BrandInput, UpdateBrandInput } from "../inputs/brandInput";
import { Query, Arg, Mutation, Resolver, UseMiddleware } from "type-graphql";
import { isAuth } from "../auth";
import { BrandResponse } from "../types/responses";
@Resolver()
export class BrandResolver{
    @Query(() => [Brand])
    async getBrands(): Promise<Brand[]>{
        return Brand.find({relations:["products"]});
    }

    @Query(() => Brand)
    async getBrand(@Arg("id") id:number):Promise<Brand|undefined>{
        try{
            return Brand.findOne(id,{relations:["products"]});
        }catch(err){
            throw new Error("Error retrieving brand")
        }
        
    }

    @Mutation(() => Brand)
    //@UseMiddleware(isAuth)
    async createBrand(@Arg ("brand") brand: BrandInput) : Promise<Brand|undefined>{
        try{
            const newBrand = await Brand.create(brand).save();
            return newBrand;
            
        }catch(err){
            console.error("Error", err);
            return undefined;
        }
    }

    @Mutation(() => BrandResponse)
    @UseMiddleware(isAuth)
    async updateBrand(
        @Arg ("brand") brand: UpdateBrandInput,
        @Arg ("id") id: number) : Promise<BrandResponse> {
        const dbBrand = await Brand.findOne(id,{relations:["products"]})
        if(!dbBrand){
            return{
                errors: [
                    {message:"Non existing brand with given id"}
                ],
            }
        }
        
        try{
            const newBrand = {
                ...dbBrand,
                ...brand
            } as Brand
            await Brand.update(id,newBrand)
            return {
                brand: newBrand
            }
            
        }catch(err){
            console.error("Error", err);
            return {
                errors: [
                    {message:"Unable to update brand"}
                ],
            }
        }
    }

}