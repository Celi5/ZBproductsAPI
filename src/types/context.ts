import { Request, Response } from 'express';
export type OwnContext = {
    req: Request 
    res: Response;
    payload?: {userId: string};
}