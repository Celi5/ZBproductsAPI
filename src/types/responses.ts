import { User } from "../entity/User";
import { Field, ObjectType } from "type-graphql";
import { Product } from "../entity/Product";
import { Brand } from "../entity/Brand";

@ObjectType()
export class FieldError {
  @Field()
  field?: string;
  @Field()
  message: string;
}

@ObjectType()
export class LoginResponse{
  @Field()
  accessToken: string;

  @Field(() => [FieldError], { nullable: true })
  errors?: FieldError[];
  
}

@ObjectType()
export class UserResponse {
  @Field(() => [FieldError], { nullable: true })
  errors?: FieldError[];

  @Field(() => User, { nullable: true })
  user?: User;
}

@ObjectType()
export class ProductResponse {
  @Field(() => [FieldError], { nullable: true })
  errors?: FieldError[];

  @Field(() => Product, { nullable: true })
  product?: Product;
}

@ObjectType()
export class BrandResponse {
  @Field(() => [FieldError], { nullable: true })
  errors?: FieldError[];

  @Field(()=>Brand, { nullable: true })
  brand?: Brand;
}