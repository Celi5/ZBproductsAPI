import { sign, verify } from "jsonwebtoken";
import { MiddlewareFn } from "type-graphql";
import { User } from "./entity/User";
import { OwnContext } from "./types/context";

export const createAccessToken = (user: User) => {
    return sign({userId: user.id}, process.env.ACCESS_TOKEN_SECRET!||"asldklakneww",
    { expiresIn: "7d"});
};
export const createRefreshToken = (user: User) => {
    return sign({userId: user.id}, process.env.REFRESH_TOKEN_SECRET!||"sndcsdosdkfj",
    { expiresIn:"7d"});
};

export const isAuth: MiddlewareFn<OwnContext> = ({context}, next)=> {
   const authorization = context.req.headers['authorization'];
   if(!authorization){
       throw new Error("Not authorized");
   }
   try{
    const token = authorization.split(" ")[1];
    const payload = verify(token,process.env.ACCESS_TOKEN_SECRET!|| "asldklakneww")
    context.payload = payload as any;
   }catch(err){
       console.error(err)
       throw new Error("Not authorized");
   }
   return next();
}

