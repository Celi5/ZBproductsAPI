import { Field, InputType } from "type-graphql";

@InputType()
export class ProductInput{
    @Field({nullable: true})
    name?: string;

    @Field({nullable: true})
    idBrand?: number;

    @Field({nullable: true})
    price?: number;

    @Field({nullable: true})
    sku?: string;

    @Field({nullable: true})
    description?: string;
}
@InputType()
export class CreateProductInput{
    @Field()
    name!: string;

    @Field()
    idBrand!: number;

    @Field()
    price!: number;

    @Field()
    sku!: string;

    @Field()
    description!: string;
}