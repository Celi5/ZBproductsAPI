import { Field, InputType } from "type-graphql";
import { MaxLength, Length } from "class-validator";

@InputType()
export class UserInput{
    @Field()
    @MaxLength(50)
    firstName!: string

    @Field()
    @MaxLength(50)
    lastName!: string

    @Field()
    @MaxLength(100)
    email!: string

    @Field()
    @Length(2,50)
    username!: string

    @Field()
    @Length(2,50)
    password!: string
}

@InputType()
export class UserPwdInput{
    @Field()
    emailUsername: string;
    @Field()
    password: string;
}

