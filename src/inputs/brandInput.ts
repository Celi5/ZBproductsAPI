import { Field, InputType } from "type-graphql";

@InputType()
export class BrandInput{
    @Field()
    name!: string;

    @Field()
    description!: string;
}

@InputType()
export class UpdateBrandInput{
    @Field({nullable: true})
    name?: string;

    @Field({nullable: true})
    description?: string;
}