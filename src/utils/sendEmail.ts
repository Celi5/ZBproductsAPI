import nodemailer from "nodemailer";
import { Product } from "../entity/Product";

// async..await is not allowed in global scope, must use a wrapper
export async function sendEmail(to: string[], userName: string, product: Product) {
  const transporter = nodemailer.createTransport({
    name: "smtp.ethereal.email",
    host: "smtp.ethereal.email",
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
      user: "hyittcff55fiopd5@ethereal.email", // generated ethereal user
      pass: "95HUr1mBYnHDhYkaYf", // generated ethereal password
    },
  });

  // send mail with defined transport object
  let info = await transporter.sendMail({
    from: userName + '<hyittcff55fiopd5@ethereal.email>', // sender address
    to: to,
    subject: "Updated Product",
    text: 'Updated Product (' + product.id + ')' + product.name
  });

  console.log("Message sent: %s", info.messageId);
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
}