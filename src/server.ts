import { ApolloServer } from "apollo-server-express";
import cors from "cors";
import express from "express";
import 'reflect-metadata';
import { __prod__ } from "./constants";
import cookieParser from "cookie-parser";
// import { Server, createServer } from "http";
import {createSchema} from './schema';
// import { OwnContext } from "./types/context";

export async function startServer(){
    const app = express();
    app.use(
      cors({
        origin:"https://studio.apollographql.com",
        credentials:true,
      }),
    );
    app.use(cookieParser());
    //const server: Server = createServer(app);
    const schema = await createSchema();

    const apolloServer = new ApolloServer({
        schema,
        context: ({req, res}: any) => ({req, res}),
    })
    await apolloServer.start();

    apolloServer.applyMiddleware({app, path:'/graphql', cors: false});

    return app;
}