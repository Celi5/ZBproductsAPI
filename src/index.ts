import "reflect-metadata";
import { createConnection } from "typeorm";
import { startServer } from "./server";
import { __prod__ } from "./constants";

const main = async () => {
  await createConnection({
    type: "postgres",
    host:process.env.DB_HOST! || "34.67.139.92",
    port:5432,
    username: "postgres",
    password: process.env.DB_PASSWORD || "4N2bsFhexjmDFHDA",
    database: "postgres",
    synchronize: true,
    entities: [
      "./entity/*.js"
   ],
   cli: {
      entitiesDir: "./entity",
      migrationsDir: "./migration",
      subscribersDir: "./subscriber"
   }
  });
  const app = await startServer();


 
  app.listen(8080);
  console.log("Starting");
};

main().catch((err) => {
  console.error("Error: ", err);
});
