import { ObjectType, Field } from "type-graphql";
import { BaseEntity, Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@ObjectType()
@Entity()
export class User extends BaseEntity {
    @Field()
    @PrimaryGeneratedColumn()
    id!: number;

    @Field()
    @Column()
    firstName!: string;

    @Field()
    @Column()
    lastName: string;

    @Field()
    @Column({default:true})
    isActive!: boolean;

    @Field()
    @Column({default:false})
    isAdmin: boolean;

    @Field()
    @Column({unique: true})
    email!: string;

    @Field()
    @Column({unique: true})
    username!: string;

    @Field()
    @Column()
    password!: string

    @Field()
    @CreateDateColumn()
    createdAt: Date;

    @Field()
    @UpdateDateColumn()
    updatedAt: Date

}