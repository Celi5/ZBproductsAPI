import { Field, ObjectType } from "type-graphql";
import { BaseEntity, Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { Brand } from "./Brand";

@ObjectType()
@Entity()
export class Product extends BaseEntity{
    @Field()
    @PrimaryGeneratedColumn()
    id:number;

    @Field()
    @Column()
    name: string;

    @Field({nullable:true})
    @Column({unique: true})
    sku: string;

    @Field()
    @Column()
    description: string;

    @Field()
    @Column()
    price: number;

    @Field()
    @Column({default:0})
    timesQueried: number;

    @Field(()=>Brand)
    @ManyToOne(()=> Brand, brand => brand.products)
    brand:Brand;

    @Field()
    @CreateDateColumn()
    createdAt: Date;

    @Field()
    @UpdateDateColumn()
    updatedAt: Date;
}