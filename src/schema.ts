import { buildSchema } from "type-graphql";
import { UserResolver } from "./resolvers/userResolver";
import { GraphQLSchema } from "graphql";
import 'reflect-metadata';
import { ProductResolver } from "./resolvers/productResolver";
import { BrandResolver } from "./resolvers/brandResolver";

export const createSchema = async (): Promise<GraphQLSchema> =>{
    try{
        return await buildSchema({
           resolvers:[
            UserResolver,
            ProductResolver,
            BrandResolver
           ],
        });
    }catch(e){console.log('Error building schema: ',e)
throw e;}
}