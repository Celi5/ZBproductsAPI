# ZBproductsAPI

## How to make queries and Mutations?

Run:
npm run dev
This will start app on localhost:4000

Head to ocalhost:4000/graphql in order to access the api. 

According to instructions, only authorized users will be allowed to do mutations.
In order to fullfill this requirement, jwt is used.

If an need-to-be-authorized (updateBrand, updateProduct) query is executed, first, you need to get the jwt for that user.
This is achieved by doing the signIn mutation. 
This retrieves an accesToken atribute which needs to be passed on *headers* as:

- Authorization: "Bearer {{token}}"

### Example query 
On route "/graphql" is specified which queries and mutations are available and what parameters they receive.
Yet, here's an example on how to make a mutation:
 ```graphql
mutation UpdateProductMutation($product: ProductInput!, $updateProductId: Float!) {
  updateProduct(product: $product, id: $updateProductId) {
   product {
     sku
   }
  }
}
``` 

While in variables, there should be something like this:
``` json
{  "product": {
  "price": 154
},
  "updateProductId": 1  
}
```

Of corse, on headers, the Auth token as indicated above.
## Tests
After you downloaded the project and have runned
 - npm i

You should be able to run tests using command:
npm run test

Tests are already set to send info to a test DB, so no DBMS is required.

### Why is not deployed?
Well, several random *super weird* problems have presented. Some bugs related to typescript, tsc; related to package.json etc. I've been hours trying to debug, trying on different environments (errors are different), asked for help and nothing seems to work properly. 
I tried on GCP and Heroku, but none of them seems to work. I will keep trying, just in case I manage to deploy the app in near future, this are links for any of them:

- GCP: https://heroic-bonbon-329513.uc.r.appspot.com
- Heroku: https://thawing-peak-41095.herokuapp.com/ 
