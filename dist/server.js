"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const apollo_server_express_1 = require("apollo-server-express");
const cors_1 = require("cors");
const express_1 = require("express");
require("reflect-metadata");
const cookie_parser_1 = require("cookie-parser");
const schema_1 = require("./schema");
async function startServer() {
    const app = express_1.default();
    app.use(cors_1.default({
        origin: "https://studio.apollographql.com",
        credentials: true,
    }));
    app.use(cookie_parser_1.default());
    const schema = await schema_1.createSchema();
    const apolloServer = new apollo_server_express_1.ApolloServer({
        schema,
        context: ({ req, res }) => ({ req, res }),
    });
    await apolloServer.start();
    apolloServer.applyMiddleware({ app, path: '/graphql', cors: false });
    return app;
}
exports.startServer = startServer;
//# sourceMappingURL=server.js.map