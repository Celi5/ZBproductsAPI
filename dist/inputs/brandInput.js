"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const type_graphql_1 = require("type-graphql");
let BrandInput = class BrandInput {
};
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", String)
], BrandInput.prototype, "name", void 0);
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", String)
], BrandInput.prototype, "description", void 0);
BrandInput = __decorate([
    type_graphql_1.InputType()
], BrandInput);
exports.BrandInput = BrandInput;
let UpdateBrandInput = class UpdateBrandInput {
};
__decorate([
    type_graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], UpdateBrandInput.prototype, "name", void 0);
__decorate([
    type_graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], UpdateBrandInput.prototype, "description", void 0);
UpdateBrandInput = __decorate([
    type_graphql_1.InputType()
], UpdateBrandInput);
exports.UpdateBrandInput = UpdateBrandInput;
//# sourceMappingURL=brandInput.js.map