"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
const typeorm_1 = require("typeorm");
const server_1 = require("./server");
const main = async () => {
    await typeorm_1.createConnection({
        type: "postgres",
        host: process.env.DB_HOST || "34.67.139.92",
        port: 5432,
        username: "postgres",
        password: process.env.DB_PASSWORD || "4N2bsFhexjmDFHDA",
        database: "postgres",
        synchronize: true,
        entities: [
            "./entity/*.js"
        ],
        cli: {
            entitiesDir: "./entity",
            migrationsDir: "./migration",
            subscribersDir: "./subscriber"
        }
    });
    const app = await server_1.startServer();
    app.listen(8080);
    console.log("Starting");
};
main().catch((err) => {
    console.error("Error: ", err);
});
//# sourceMappingURL=index.js.map