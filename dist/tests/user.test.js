"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const gCall_1 = require("../test.utils/gCall");
const testConn_1 = require("../test.utils/testConn");
const faker_1 = require("faker");
const User_1 = require("../entity/User");
let conn;
beforeAll(async () => {
    conn = await testConn_1.testConn();
});
afterAll(async () => {
    conn.close();
});
const signUpMutation = `
mutation SignUpMutation($user: UserInput!) {
    signUp(user: $user) {
    id
    firstName
    lastName
    username
    email  
    }
  }`;
describe("UserResolver", () => {
    it("create User", async () => {
        const user = {
            firstName: faker_1.default.name.firstName(),
            lastName: faker_1.default.name.lastName(),
            email: faker_1.default.internet.email(),
            username: faker_1.default.internet.userName(),
            password: faker_1.default.internet.password(),
        };
        const response = await gCall_1.gCall({
            source: signUpMutation,
            variableValues: {
                user,
            },
        });
        expect(response).toMatchObject({
            data: {
                signUp: {
                    firstName: user.firstName,
                    lastName: user.lastName,
                    username: user.username,
                    email: user.email,
                },
            },
        });
        const dbUser = await User_1.User.findOne({ where: { email: user.email } });
        expect(dbUser).toBeDefined();
    });
});
//# sourceMappingURL=user.test.js.map