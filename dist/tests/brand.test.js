"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const gCall_1 = require("../test.utils/gCall");
const testConn_1 = require("../test.utils/testConn");
const faker_1 = require("faker");
const Brand_1 = require("../entity/Brand");
let conn;
beforeAll(async () => {
    conn = await testConn_1.testConn();
});
afterAll(async () => {
    conn.close();
});
const createBrandMutation = `
mutation CreateBrandMutation($brand: BrandInput!) {
    createBrand(brand: $brand) {
      name
      description
    }
  }`;
describe("BrandResolver", () => {
    it("create Brand", async () => {
        const brand = {
            name: faker_1.default.internet.avatar(),
            description: faker_1.default.lorem.text(),
        };
        const response = await gCall_1.gCall({
            source: createBrandMutation,
            variableValues: {
                brand,
            },
        });
        console.log("==RESBRAND==", response);
        expect(response).toMatchObject({
            data: {
                createBrand: {
                    name: brand.name,
                    description: brand.description,
                },
            },
        });
        const dbBrand = await Brand_1.Brand.findOne({ where: { name: brand.name } });
        expect(dbBrand).toBeDefined();
    });
});
//# sourceMappingURL=brand.test.js.map