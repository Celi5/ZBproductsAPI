"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const gCall_1 = require("../test.utils/gCall");
const testConn_1 = require("../test.utils/testConn");
const faker_1 = require("faker");
const Product_1 = require("../entity/Product");
const Brand_1 = require("../entity/Brand");
let conn;
beforeAll(async () => {
    conn = await testConn_1.testConn();
});
afterAll(async () => {
    conn.close();
});
const createProductMutation = `
mutation CreateProductMutation($product: CreateProductInput!) {
    createProduct(product: $product) {
      sku
      name
      description
    }
  }`;
describe("ProductResolver", () => {
    it("create Product", async () => {
        const brand = await Brand_1.Brand.create({
            name: "Tortas Filos",
            description: faker_1.default.lorem.sentence(),
        }).save();
        if (!brand) {
            throw new Error("Non valid idBrand for relations");
        }
        const product = {
            name: faker_1.default.lorem.word(),
            description: faker_1.default.lorem.sentence(),
            price: 100,
            sku: faker_1.default.random.word(),
            idBrand: brand.id,
        };
        const response = await gCall_1.gCall({
            source: createProductMutation,
            variableValues: {
                product,
            },
        });
        console.log("==RESPROD==", response);
        expect(response).toMatchObject({
            data: {
                createProduct: {
                    sku: product.sku,
                    name: product.name,
                    description: product.description,
                },
            },
        });
        const dbProduct = await Product_1.Product.findOne({ where: { sku: product.sku } });
        expect(dbProduct).toBeDefined();
    });
});
//# sourceMappingURL=product.test.js.map