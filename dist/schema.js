"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const type_graphql_1 = require("type-graphql");
const userResolver_1 = require("./resolvers/userResolver");
require("reflect-metadata");
const productResolver_1 = require("./resolvers/productResolver");
const brandResolver_1 = require("./resolvers/brandResolver");
exports.createSchema = async () => {
    try {
        return await type_graphql_1.buildSchema({
            resolvers: [
                userResolver_1.UserResolver,
                productResolver_1.ProductResolver,
                brandResolver_1.BrandResolver
            ],
        });
    }
    catch (e) {
        console.log('Error building schema: ', e);
        throw e;
    }
};
//# sourceMappingURL=schema.js.map