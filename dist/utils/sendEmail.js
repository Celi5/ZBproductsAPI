"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const nodemailer_1 = require("nodemailer");
async function sendEmail(to, userName, product) {
    let testAccount = await nodemailer_1.default.createTestAccount();
    console.log(testAccount);
    const transporter = nodemailer_1.default.createTransport({
        host: "smtp.ethereal.email",
        port: 587,
        secure: false,
        auth: {
            user: testAccount.user,
            pass: testAccount.pass,
        },
    });
    let info = await transporter.sendMail({
        from: userName + '<celis.mal.ivan@example.com>',
        to: to,
        subject: "Updated Product",
        text: 'Updated Product (' + product.id + ')' + product.name
    });
    console.log("Message sent: %s", info.messageId);
    console.log("Preview URL: %s", nodemailer_1.default.getTestMessageUrl(info));
}
exports.sendEmail = sendEmail;
//# sourceMappingURL=sendEmail.js.map