"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
exports.testConn = (drop = false) => {
    return typeorm_1.createConnection({
        type: "postgres",
        host: process.env.DB_HOST || "34.136.118.228",
        port: 5432,
        username: "postgres",
        password: process.env.DB_PASSWORD || "2aIenrNFGtvEzBh8",
        database: "postgres",
        synchronize: drop,
        dropSchema: drop,
        entities: [
            "src/entity/**/*.ts"
        ],
        cli: {
            entitiesDir: "src/entity",
            migrationsDir: "src/migration",
            subscribersDir: "src/subscriber"
        }
    });
};
//# sourceMappingURL=testConn.js.map