"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_1 = require("graphql");
const schema_1 = require("../schema");
let schema;
exports.gCall = async ({ source, variableValues }) => {
    if (!schema) {
        schema = await schema_1.createSchema();
    }
    return graphql_1.graphql({
        schema,
        source,
        variableValues
    });
};
//# sourceMappingURL=gCall.js.map