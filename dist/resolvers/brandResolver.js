"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const Brand_1 = require("../entity/Brand");
const brandInput_1 = require("../inputs/brandInput");
const type_graphql_1 = require("type-graphql");
const auth_1 = require("../auth");
let BrandResolver = class BrandResolver {
    async getBrands() {
        return Brand_1.Brand.find({ relations: ["products"] });
    }
    async getBrand(id) {
        try {
            return Brand_1.Brand.findOne(id, { relations: ["products"] });
        }
        catch (err) {
            throw new Error("Error retrieving brand");
        }
    }
    async createBrand(brand) {
        try {
            const newBrand = await Brand_1.Brand.create(brand).save();
            return newBrand;
        }
        catch (err) {
            console.error("Error", err);
            return undefined;
        }
    }
    async updateBrand(brand, id) {
        const dbBrand = await Brand_1.Brand.findOne(id, { relations: ["products"] });
        if (!dbBrand) {
            return {
                errors: [
                    { message: "Non existing brand with given id" }
                ],
            };
        }
        try {
            const newBrand = Object.assign({}, dbBrand, brand);
            await Brand_1.Brand.update(id, newBrand);
            return {
                brand: newBrand
            };
        }
        catch (err) {
            console.error("Error", err);
            return {
                errors: [
                    { message: "Unable to update brand" }
                ],
            };
        }
    }
};
__decorate([
    type_graphql_1.Query(() => [Brand_1.Brand]),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], BrandResolver.prototype, "getBrands", null);
__decorate([
    type_graphql_1.Query(() => Brand_1.Brand),
    __param(0, type_graphql_1.Arg("id")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], BrandResolver.prototype, "getBrand", null);
__decorate([
    type_graphql_1.Mutation(() => Brand_1.Brand),
    __param(0, type_graphql_1.Arg("brand")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [brandInput_1.BrandInput]),
    __metadata("design:returntype", Promise)
], BrandResolver.prototype, "createBrand", null);
__decorate([
    type_graphql_1.Mutation(() => Brand_1.Brand),
    type_graphql_1.UseMiddleware(auth_1.isAuth),
    __param(0, type_graphql_1.Arg("brand")),
    __param(1, type_graphql_1.Arg("id")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [brandInput_1.UpdateBrandInput, Number]),
    __metadata("design:returntype", Promise)
], BrandResolver.prototype, "updateBrand", null);
BrandResolver = __decorate([
    type_graphql_1.Resolver()
], BrandResolver);
exports.BrandResolver = BrandResolver;
//# sourceMappingURL=brandResolver.js.map