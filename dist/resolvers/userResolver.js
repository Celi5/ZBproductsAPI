"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const User_1 = require("../entity/User");
const type_graphql_1 = require("type-graphql");
const userInput_1 = require("../inputs/userInput");
const responses_1 = require("../types/responses");
const bcryptjs_1 = require("bcryptjs");
const auth_1 = require("../auth");
let UserResolver = class UserResolver {
    async getUser(id) {
        const user = await User_1.User.findOne({
            where: [{ id: id }],
        });
        return user;
    }
    async signUp(user) {
        const hashedPass = await bcryptjs_1.hash(user.password, 12);
        user.password = hashedPass;
        const newUser = await User_1.User.create(user).save();
        return newUser;
    }
    async updateUser(id, user) {
        const dbUser = await User_1.User.findOne(id);
        if (!dbUser) {
            return {
                errors: [{ message: "Non existing user for given id" }],
            };
        }
        const updatedData = Object.assign({}, dbUser, user);
        try {
            await User_1.User.update(id, updatedData);
            return {
                user: updatedData,
            };
        }
        catch (err) {
            console.error(err);
            return {
                errors: [{ message: "Unable to update user." }],
            };
        }
    }
    async signIn(user, { res }) {
        const dbUser = await User_1.User.findOne(user.emailUsername.includes("@")
            ? { where: { email: user.emailUsername } }
            : { where: { username: user.emailUsername } });
        if (dbUser) {
            const checkPass = await bcryptjs_1.compare(user.password, dbUser.password);
            if (checkPass) {
                res.cookie('qid', auth_1.createRefreshToken(dbUser), {
                    httpOnly: true,
                    sameSite: 'none',
                    secure: false,
                });
                return {
                    accessToken: auth_1.createAccessToken(dbUser)
                };
            }
        }
        throw new Error("Wrong credentials");
    }
};
__decorate([
    type_graphql_1.Query(() => User_1.User),
    __param(0, type_graphql_1.Arg("id")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "getUser", null);
__decorate([
    type_graphql_1.Mutation(() => User_1.User),
    __param(0, type_graphql_1.Arg("user")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [userInput_1.UserInput]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "signUp", null);
__decorate([
    type_graphql_1.Mutation(() => responses_1.UserResponse),
    __param(0, type_graphql_1.Arg("id")),
    __param(1, type_graphql_1.Arg("user")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, userInput_1.UserInput]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "updateUser", null);
__decorate([
    type_graphql_1.Mutation(() => responses_1.LoginResponse),
    __param(0, type_graphql_1.Arg("user")),
    __param(1, type_graphql_1.Ctx()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [userInput_1.UserPwdInput, Object]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "signIn", null);
UserResolver = __decorate([
    type_graphql_1.Resolver(() => User_1.User)
], UserResolver);
exports.UserResolver = UserResolver;
//# sourceMappingURL=userResolver.js.map