"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const Product_1 = require("../entity/Product");
const productInput_1 = require("../inputs/productInput");
const type_graphql_1 = require("type-graphql");
const Brand_1 = require("../entity/Brand");
const sendEmail_1 = require("../utils/sendEmail");
const User_1 = require("../entity/User");
const auth_1 = require("../auth");
let ProductResolver = class ProductResolver {
    async getProducts() {
        return Product_1.Product.find({ relations: ["brand"] });
    }
    async getProduct(id) {
        await this.updateTimesQueried(id);
        return Product_1.Product.findOne(id, { relations: ["brand"] });
    }
    async updateTimesQueried(id) {
        const product = await Product_1.Product.findOne(id);
        if (product) {
            product.timesQueried++;
            const updatedData = Object.assign({}, product);
            try {
                await Product_1.Product.update(id, updatedData);
            }
            catch (err) {
                throw new Error("Problem updating product");
            }
        }
    }
    async createProduct(product) {
        try {
            const brand = await Brand_1.Brand.findOne(product.idBrand);
            if (brand) {
                const newProduct = Product_1.Product.create(product);
                newProduct.brand = brand;
                await Product_1.Product.save(newProduct);
                return newProduct;
            }
            else {
                return undefined;
            }
        }
        catch (err) {
            console.error("Error", err);
            return undefined;
        }
    }
    async updateProduct(id, product, { payload }) {
        let productUpdate = await Product_1.Product.findOne(id);
        let brand;
        if (!productUpdate) {
            return {
                errors: [
                    { field: "id", message: "Non existing product with given id." }
                ]
            };
        }
        if (product.idBrand) {
            if (brand) {
                productUpdate.brand = brand;
            }
            else {
                return {
                    errors: [{ message: "Non existing brand with given id" }]
                };
            }
        }
        const updatedData = Object.assign({}, productUpdate, product);
        try {
            await Product_1.Product.update(id, updatedData);
            const idUser = +payload.userId;
            const dbUser = await User_1.User.findOne(idUser);
            const userMails = await this.getUsersEmail();
            if (!dbUser) {
                return {
                    errors: [{ message: "Not authorized user" }]
                };
            }
            const userName = dbUser.firstName + dbUser.lastName;
            await sendEmail_1.sendEmail(userMails, userName, updatedData);
            console.log(updatedData);
            return { product: updatedData };
        }
        catch (err) {
            console.log(err);
            return {
                errors: [
                    { message: "Unable to update product: " + err.message }
                ]
            };
        }
    }
    async getUsersEmail() {
        const users = await User_1.User.find();
        let usersEmails;
        usersEmails = [];
        users.forEach(element => {
            usersEmails.push(element.email);
        });
        return usersEmails;
    }
};
__decorate([
    type_graphql_1.Query(() => [Product_1.Product]),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], ProductResolver.prototype, "getProducts", null);
__decorate([
    type_graphql_1.Query(() => Product_1.Product),
    __param(0, type_graphql_1.Arg("id")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ProductResolver.prototype, "getProduct", null);
__decorate([
    type_graphql_1.Mutation(() => Product_1.Product),
    __param(0, type_graphql_1.Arg("product")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [productInput_1.CreateProductInput]),
    __metadata("design:returntype", Promise)
], ProductResolver.prototype, "createProduct", null);
__decorate([
    type_graphql_1.Mutation(() => Product_1.Product),
    type_graphql_1.UseMiddleware(auth_1.isAuth),
    __param(0, type_graphql_1.Arg("id")),
    __param(1, type_graphql_1.Arg("product")),
    __param(2, type_graphql_1.Ctx()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, productInput_1.ProductInput, Object]),
    __metadata("design:returntype", Promise)
], ProductResolver.prototype, "updateProduct", null);
ProductResolver = __decorate([
    type_graphql_1.Resolver()
], ProductResolver);
exports.ProductResolver = ProductResolver;
//# sourceMappingURL=productResolver.js.map